package cositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Moduls {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	
	@Column
	private int modul;
	
	@Column
	private int hores;
	
	@Column
	private String nom;
	
	@ManyToOne
	private Cicle cicle;

	public Moduls() {
		super();
	}

	public Moduls(int modul, int hores, String nom, Cicle cicle) {
		super();
		this.modul = modul;
		this.hores = hores;
		this.nom = nom;
		this.cicle = cicle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModul() {
		return modul;
	}

	public void setModul(int modul) {
		this.modul = modul;
	}

	public int getHores() {
		return hores;
	}

	public void setHores(int hores) {
		this.hores = hores;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Cicle getCicle() {
		return cicle;
	}

	public void setCicle(Cicle cicle) {
		this.cicle = cicle;
	}

	@Override
	public String toString() {
		return "Moduls [id=" + id + ", modul=" + modul + ", hores=" + hores + ", nom=" + nom + ", cicle=" + cicle + "]";
	}
	
	
	
	

}
