package cositas;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Cicle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	
	@Column
	private String nom;
	
	@Column
	private String descripcio;
	
	@Column
	private char nivell;
	
	@OneToMany(mappedBy="cicle")
	private List<Moduls> moduls;

	public Cicle( String nom, String descripcio, char nivell) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.nivell = nivell;
		this.moduls= new ArrayList<Moduls>();
	}

	public Cicle() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public char getNivell() {
		return nivell;
	}

	public void setNivell(char nivell) {
		this.nivell = nivell;
	}

	
	
	
	
	public List<Moduls> getModuls() {
		return moduls;
	}

	public void setModuls(List<Moduls> moduls) {
		this.moduls = moduls;
	}

	@Override
	public String toString() {
		return "Cicle [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", nivell=" + nivell + "]";
	}

	
	
	
}
