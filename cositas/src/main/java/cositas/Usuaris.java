package cositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Usuaris {
	//clave priamria
	@Id
		//autonumerico
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		//columna
	@Column
	private int idUsuari;
	
	@Column
	private String nomUsuari;
	
	@Column
	private String cognom1Usuari;
	
	@Column
	private String emailUsuari;
	
	@Column
	private String password;
	
	@Column
	private int idGrup;
	
	@Column
	private char rol;
	
	@Column
	private boolean actiu;
	
	@Column
	private boolean repetidor;
	
	@Column
	private String cognom2Usuari;
	
	@Column
	private boolean bloquejat;
	
	@Column
	private int intents;
	
	@Column
	private char sexe;
	
	
	
	public Usuaris() {
		super();
	}



	public Usuaris(String nomUsuari, String cognom1Usuari, String emailUsuari, String password,
			int idGrup, char rol, boolean actiu, boolean repetidor, String cognom2Usuari, boolean bloquejat,
			int intents, char sexe) {
		super();
		this.nomUsuari = nomUsuari;
		this.cognom1Usuari = cognom1Usuari;
		this.emailUsuari = emailUsuari;
		this.password = password;
		this.idGrup = idGrup;
		this.rol = rol;
		this.actiu = actiu;
		this.repetidor = repetidor;
		this.cognom2Usuari = cognom2Usuari;
		this.bloquejat = bloquejat;
		this.intents = intents;
		this.sexe = sexe;
	}



	public int getIdUsuari() {
		return idUsuari;
	}



	public void setIdUsuari(int idUsuari) {
		this.idUsuari = idUsuari;
	}



	public String getNomUsuari() {
		return nomUsuari;
	}



	public void setNomUsuari(String nomUsuari) {
		this.nomUsuari = nomUsuari;
	}



	public String getCognom1Usuari() {
		return cognom1Usuari;
	}



	public void setCognom1Usuari(String cognom1Usuari) {
		this.cognom1Usuari = cognom1Usuari;
	}



	public String getEmailUsuari() {
		return emailUsuari;
	}



	public void setEmailUsuari(String emailUsuari) {
		this.emailUsuari = emailUsuari;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public int getIdGrup() {
		return idGrup;
	}



	public void setIdGrup(int idGrup) {
		this.idGrup = idGrup;
	}



	public char getRol() {
		return rol;
	}



	public void setRol(char rol) {
		this.rol = rol;
	}



	public boolean isActiu() {
		return actiu;
	}



	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}



	public boolean isRepetidor() {
		return repetidor;
	}



	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}



	public String getCognom2Usuari() {
		return cognom2Usuari;
	}



	public void setCognom2Usuari(String cognom2Usuari) {
		this.cognom2Usuari = cognom2Usuari;
	}



	public boolean isBloquejat() {
		return bloquejat;
	}



	public void setBloquejat(boolean bloquejat) {
		this.bloquejat = bloquejat;
	}



	public int getIntents() {
		return intents;
	}



	public void setIntents(int intents) {
		this.intents = intents;
	}



	public char getSexe() {
		return sexe;
	}



	public void setSexe(char sexe) {
		this.sexe = sexe;
	}



	@Override
	public String toString() {
		return "Usuaris [idUsuari=" + idUsuari + ", nomUsuari=" + nomUsuari + ", cognom1Usuari=" + cognom1Usuari
				+ ", emailUsuari=" + emailUsuari + ", password=" + password + ", idGrup=" + idGrup + ", rol=" + rol
				+ ", actiu=" + actiu + ", repetidor=" + repetidor + ", cognom2Usuari=" + cognom2Usuari + ", bloquejat="
				+ bloquejat + ", intents=" + intents + ", sexe=" + sexe + "]";
	}
	
	
	
	
	
}
