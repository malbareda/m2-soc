package cositas;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class MainConMenu {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int opcion=1;
		while (opcion!=0) {
			System.out.println("Dime opcion");
			opcion = sc.nextInt();
			sc.nextLine();
			switch (opcion) {
			
			case 1:
				
				session = getSessionFactory().openSession();

				System.out.println("añadir cosa");
				System.out.println("dime cosa");
				String n = sc.nextLine();
				System.out.println("dime nivel");
				int niv = sc.nextInt();
				sc.nextLine();
				System.out.println("dime brasa");
				boolean b = sc.nextBoolean();
				sc.nextLine();
				
				Cosas c = new Cosas(n, niv, b);
				
				//abro una transaccion
				session.beginTransaction();
				session.saveOrUpdate(c);
				session.getTransaction().commit();
				session.close();
				break;
				
			case 2:
				session = getSessionFactory().openSession();
				List<Cosas> todasLasCosas = session.createQuery("FROM Cosas").list();
				int contador =0;
				for(int i=0;i<todasLasCosas.size();i++) {
					Cosas cosita = todasLasCosas.get(i);
					if(cosita.isNosVaADarLaBrasaSobreEsto()==true) {
						System.out.println(cosita);
						contador++;
					}
				}
				System.out.println("Hay "+contador+ " cosas que sabe Sergio que nos va a dar la brasa sobre ello");
				session.close();

			case 0: break;
			}
		}

	}

}
