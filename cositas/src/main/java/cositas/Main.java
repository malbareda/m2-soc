package cositas;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class Main {
	
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void main(String[] args) {
		
		//abro una sesion
		session = getSessionFactory().openSession();

		//abro una transaccion
		session.beginTransaction();
		
		Cosas cosa = new Cosas("Que Bassou odia a Marc", 100, false);
		
		//session.saveOrUpdate(cosa);
		
		Cosas cosa2 = new Cosas("Que los procesadores blablabal", 70, true);
		
		//session.saveOrUpdate(cosa2);
		
		//guaradme todos los cambios y cierra la transaccion
		session.getTransaction().commit();

		
		
		//segunda transaccion
		session.beginTransaction();
		
		//Cosas cosaQueQuieroModificar = session.get(Cosas.class, 2);
		//cosaQueQuieroModificar.setNombre("sabias que los procesadores no funcionan con 1.38 volts ya esta");
		//session.saveOrUpdate(cosaQueQuieroModificar);
		
		session.getTransaction().commit();
		
		
		
		session.close();
		
		System.out.println("Todo ha salido a pedir de Milhouse");
		
		
		
	}


}
