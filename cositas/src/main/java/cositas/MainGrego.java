package cositas;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class MainGrego {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	
	public static void main(String[] args) {
		
		//abro una sesion
		session = getSessionFactory().openSession();

		//abro una transaccion
		session.beginTransaction();
		
		Usuaris u = new Usuaris("Bassou", "Oumira", "odioAMarc@gmail.com", "hate", 1, 'A', true, false, "MuyLargo", false,0,'H');
		
		session.saveOrUpdate(u);
		
		Cicle soc = new Cicle("SOC", "SOC-programacio sistemes",'3');
		
		
		Cicle dam = new Cicle("DAM", "DAM DAM DAM",'S');

		Moduls m2 = new Moduls(2,200,"M2",soc);
		soc.getModuls().add(m2);
		Moduls m1 = new Moduls(1,300,"M1",soc);
		soc.getModuls().add(m1);
		
		
		Moduls sistemes = new Moduls(1,132,"Sistemes Informatics",dam);
		dam.getModuls().add(sistemes);
		
		session.saveOrUpdate(sistemes);
		session.saveOrUpdate(dam);
		
		
		session.saveOrUpdate(soc);
		session.saveOrUpdate(m1);
		session.saveOrUpdate(m2);
		
		
		session.getTransaction().commit();
		
		System.out.println("Todo ha salido a pedir de Milhouse");
		
		
		Cicle c = session.get(Cicle.class, 2);
		for(int i=0;i<c.getModuls().size();i++) {
			System.out.println(c.getModuls().get(i).getNom());
		}
	}

}
