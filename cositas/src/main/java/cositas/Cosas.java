package cositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//todo lo realcionado con el ORM
//va por anotaciones

@Entity
@Table(name = "cosas_que_sabe_Sergio")
public class Cosas {

	//clave priamria
	@Id
	//autonumerico
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//columna
	@Column(name="id_cosa")
	private int id;
	
	@Column
	private String nombre;
	
	@Column
	private int NivelDeConocimientos;
	
	@Column
	private boolean NosVaADarLaBrasaSobreEsto;

	
	//dos constructores, uno vaciop y uno con todo
	public Cosas() {
		super();
	}



	public Cosas(String nombre, int nivelDeConocimientos, boolean nosVaADarLaBrasaSobreEsto) {
		super();
		this.nombre = nombre;
		NivelDeConocimientos = nivelDeConocimientos;
		NosVaADarLaBrasaSobreEsto = nosVaADarLaBrasaSobreEsto;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public int getNivelDeConocimientos() {
		return NivelDeConocimientos;
	}



	public void setNivelDeConocimientos(int nivelDeConocimientos) {
		NivelDeConocimientos = nivelDeConocimientos;
	}



	public boolean isNosVaADarLaBrasaSobreEsto() {
		return NosVaADarLaBrasaSobreEsto;
	}



	public void setNosVaADarLaBrasaSobreEsto(boolean nosVaADarLaBrasaSobreEsto) {
		NosVaADarLaBrasaSobreEsto = nosVaADarLaBrasaSobreEsto;
	}



	@Override
	public String toString() {
		return "Cosas [id=" + id + ", nombre=" + nombre + ", NivelDeConocimientos=" + NivelDeConocimientos
				+ ", NosVaADarLaBrasaSobreEsto=" + NosVaADarLaBrasaSobreEsto + "]";
	}
	
	
	
	
	
	
}
