package unPoquitoDeOrientacionAObjetos;

public class MainPanini {
	
	public static void main(String[] args) {
		
		Panini pan = new Panini(TipoPanini.JAMONYQUESO, 1, true);
		
		System.out.println("Daniel Tinini quiere un Panini");

		
		definirPanini(pan);
		
		pan.recienHecho=false;
		
		
		Panini panBassou = new Panini(TipoPanini.ATUN, 1, true);
		
		panBassou.precio=1;
		panBassou.tipo = TipoPanini.ATUN;
		panBassou.recienHecho = true;
		
		panBassou.pasarTiempo();
		
		definirPanini(panBassou);
		definirPanini(pan);
		

		
	}
	
	
	public static void definirPanini(Panini pan){
		System.out.println("El Panini es de "+pan.tipo+" y cuesta "+pan.precio+" euros");
		if(pan.recienHecho==true) {
			System.out.println("Y ADEMAS ESTA RECIEN HECHO");
		}
	}

}
