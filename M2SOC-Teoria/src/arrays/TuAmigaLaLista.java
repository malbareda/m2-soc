package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TuAmigaLaLista {
	
	
	public static void main(String[] args) {
		
		//Declaracion de la lista
		//ArrayList<TipoDeLoQueHayDentro> nombreLista = new ArrayList<TipoDeLoQueHayDentro>();
		ArrayList<Integer> lista1 = new ArrayList<Integer>();
		
		//las listas tienen un tamaño dinamico
		
		
		Random r = new Random();
		
		for(int i=0;i<10;i++) {
			int numeroRandom = r.nextInt(0,10);
			lista1.add(numeroRandom);//el add por defecto añade al final
		}
		
		System.out.println(lista1);
		
		//en la posicion 0, añademe un 5
		lista1.add(0,5);
		
		System.out.println(lista1);
		
		lista1.remove(4);
		System.out.println(lista1);
		
		
		//obtener
		
		int primerElemento = lista1.get(0);
		System.out.println(primerElemento);
		//modificar
		
		//modificar el segundo elemento
		lista1.set(1, primerElemento*10);
		
		System.out.println(lista1);
		
		//sumar 1 a todos los elementos de la lista
		for(int i=0;i<lista1.size();i++) {
			lista1.set(i, lista1.get(i)+1);
		}
		
		
		//indexOf
		
		ArrayList<String> listaStrings = new ArrayList<>();
		listaStrings.add("coche");
		listaStrings.add("moto");
		listaStrings.add("fimosis");
		listaStrings.add("manzana");
		listaStrings.add("escoba");
		listaStrings.add("3");
		
		System.out.println(listaStrings);
		
		//el remove es por posicion O por elemento
		listaStrings.remove("fimosis");
		System.out.println(listaStrings);
		
		//indexOf
		System.out.println(listaStrings.indexOf("3"));
		
		
		
	}

}
