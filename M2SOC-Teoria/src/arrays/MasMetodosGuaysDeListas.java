package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MasMetodosGuaysDeListas {
	
	public static void main(String[] args) {
		
		
		ArrayList<Integer> lista = new ArrayList<Integer>();
		
		Random r = new Random();
		for(int i=0;i<10;i++) {
			lista.add(r.nextInt(0,100));
		}
		
		System.out.println(lista);
		
		//contains. Deveulve un booleano si ese elemento esta contenido en la lista
		if(lista.contains(26)) {
			System.out.println("Has pensado lo mismo que yo");
		}else {
			System.out.println("Terrorista");
		}

		Collections.reverse(lista);
		System.out.println(lista);
		System.out.println("antes de la ordenacion "+lista.get(0));
		//Collections.sort
		Collections.sort(lista);
		System.out.println(lista);
		System.out.println("despues de la ordenacion "+lista.get(0));
		
		
		
	}

}
