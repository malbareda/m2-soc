package arrays;

import java.util.Random;

public class TuAmigoElArray {

	public static void main(String[] args) {
		
		//declaracion de un array
		//tipoDeCadaVariable[] nombre del array = new tipoDeCadaVariable[TAMAÑO];
		int n = 5;
		int[] a = new int[n];
		
		//primer elemento del array es 0
		//ultimo elemento del array es longitud-1
		
		a[0] = 4;
		a[1] = 5;
		a[2] = 7;
		a[3] = 0;
		a[4] = -1;
		System.out.println(a[0]);
		
		//Random
		Random r = new Random();
		a[0] = r.nextInt(0,10);
		System.out.println(a[0]);
		//bucle de recorrer un array de inicio a final
		for(int i = 0; i <a.length; i++) {
			a[i] = r.nextInt(0,100);
		}
		System.out.println("leer el array al reves");
		//bucle de recorrer un array de final a inicio
		for(int i = a.length-1; i >= 0; i--) {
			System.out.println(a[i]);
		}
		
		
		//imprimirUnArray
		for(int i = 0; i <a.length; i++) {
			System.out.print(a[i]+" ");
		}System.out.println();
		
	}
}
