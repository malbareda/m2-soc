package arrays;

import java.util.Scanner;

public class RepasoArrays {
	
	public static void main(String[] args) {
		
		
		//declaracion
		//tipoQueContieneElArray[] nombre = new tipoQueContieneElArray[TAMAÑO];
		
		int[] ai = new int[10];
		String[] as = new String[5];
		
		//acceder a posiciones de un array
		//nombre[numeroDeLaPosicion]
		//IMPORTANTE: La primera es 0
		//Por tanto, la ultima es array.length-1;
		
		ai[0]= 5; //Pon un 5 en la primera posicion
		as[1] = "Albert"; //Pon "Albert" en la segunda posicion
		//as[5] = "Sergio"; <-- Esto da error
		
		//BUCLE DE RECORRER ARRAYS
		for (int i = 0; i < ai.length; i++) {
			//haces lo que quieres
		}
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Dime 10 numeros");
		for (int i = 0; i < ai.length; i++) {
			ai[i] = sc.nextInt();
		}
		
		for (int i = 0; i < ai.length; i++) {
			System.out.print(ai[i]+" ");
		}System.out.println();
		System.out.println("Albert");
		
		
		System.out.println("Quiero contar cuantos 8 hay en este array de numeros");
		int contador = 0;
		for (int i = 0; i < ai.length; i++) {
			if(ai[i]==8) {
				contador++;
			}
		}
		System.out.println(contador);
		
		
		
		
	}
	
	

}
