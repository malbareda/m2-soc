package diccionaris;

import java.util.HashMap;
import java.util.Scanner;

public class salman2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		for (int cas = 0; cas < ncasos; cas++) {
			HashMap<String, String> amic = new HashMap<>();
			int n = sc.nextInt();
			sc.nextLine();
			for (int i = 0; i < n-1; i++) {
				String[] linea = sc.nextLine().split(" ");
				amic.put(linea[0], linea[1]);
				amic.put(linea[1], linea[0]);
			}
			String c = sc.nextLine();
			System.out.println(amic.get(c));
		}
	}

}
