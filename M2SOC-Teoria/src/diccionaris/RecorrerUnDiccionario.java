package diccionaris;

import java.util.HashMap;

public class RecorrerUnDiccionario {
	
	public static void main(String[] args) {
		
		
		//HashMap<Tipo de la Clave, Tipo del Valor> nombre = new HashMap<>();
		HashMap<String,String> dicc1 = new HashMap<>();
		
		
		/*
		 *Frases miticas
		 * 
		 */
		
		//put coloca una entrada en el diccionario
		dicc1.put("Bassou", "Odio al Marc");
		dicc1.put("Marc", "El dia de no se que de las nininininipanininininininini");
		dicc1.put("Albert", "Vamo a pillar paninis");
		dicc1.put("Dani", "A Dani Tinini le Dan un panini");
		
		
		
		int contadorPaninis = 0;
		//esto se le llama un foreach
		for (String llave : dicc1.keySet()) {
			System.out.println("llave: "+llave);
			String valor = dicc1.get(llave);
			System.out.println("valor: "+valor);
			if(valor.contains("panini")) {
				contadorPaninis++;
			}
		}
		System.out.println("Hay "+contadorPaninis+" frases que tienen que ver con Paninis");
		
		
	}

}
