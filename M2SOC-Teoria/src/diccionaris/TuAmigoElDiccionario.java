package diccionaris;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class TuAmigoElDiccionario {
	
	public static void main(String[] args) {
		
		
		//Declaracion
		//HashMap<Tipo de la Clave, Tipo del Valor> nombre = new HashMap<>();
		HashMap<String,String> dicc1 = new HashMap<>();
		
		
		/*
		 *Frases miticas
		 * 
		 */
		
		//put coloca una entrada en el diccionario
		dicc1.put("Bassou", "Odio al Marc");
		dicc1.put("Marc", "El dia de no se que de las mujeres");
		dicc1.put("Albert", "Vamo a pillar paninis");
		dicc1.put("Dani", "Mi profesor del Copernic no explica nada");
		
		
		System.out.println(dicc1);
		
		System.out.println(dicc1.get("Bassou"));
		
		//las claves son unicas
		dicc1.put("Bassou", "Me encanta C++");
		//el put si no existe la clave, la añade
		//si existe, sustituye el valor
		
		System.out.println(dicc1.get("Bassou"));
		
		//un get de una clave que no existe da Null
		System.out.println(dicc1.get("Sergio"));
		
		
		//mirar si lo contine antes de hacer el get
		if(dicc1.containsKey("Sergio")) {
			System.out.println(dicc1.get("Sergio"));
			
		}else {
			System.out.println("no hay sergio");
		}
		
	}

}
