package diccionaris;
import java.util.HashMap;
import java.util.Scanner;

public class Pruebas {
	static Scanner sc =  new Scanner(System.in);
	public static void main(String[] args) {
		int x = sc.nextInt();
		for (int i=0;i<x;i++) {
			int vegades = sc.nextInt();sc.nextLine();
			HashMap<String, String> hitler = new HashMap<>();
			for(int g=0; g<vegades;g++) {
				String frase = sc.nextLine();
				String[] split = frase.split("->");
				hitler.put(split[0], split[1]);
			}
			String inici = sc.nextLine();
			String desti = "";
			int acc = 0;
			while(!desti.equals("HITLER")&&acc<vegades) {
				desti = hitler.get(inici);
				acc++;
				for (String a : hitler.keySet()) {
					if(a.equals(desti)) {
						inici = a;
					}
				}
			}
			if(acc==vegades) {
				System.out.println("IMPOSSIBLE");
			} else {
				System.out.println(acc);
			}
		}
	}
}