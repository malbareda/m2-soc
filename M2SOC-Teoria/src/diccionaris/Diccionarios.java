package diccionaris;

import java.util.HashMap;
import java.util.Scanner;

public class Diccionarios {
	
	public static void main(String[] args) {
		
		HashMap<String,Integer> faltasEnFol = new HashMap<String,Integer>();
		
		
		faltasEnFol.put("SergioLopez", 0);
		faltasEnFol.put("Dani", 0);
		faltasEnFol.put("Albert", 0);
		faltasEnFol.put("Bassou", 0);
		faltasEnFol.put("Xavi", 0);
		faltasEnFol.put("SergioLuque", 0);
		faltasEnFol.put("Nkiru", 0);
		
		System.out.println("Pon el nombre de la persona que ha faltado");
		Scanner sc = new Scanner(System.in);
		String nombre = sc.nextLine();
		if(faltasEnFol.containsKey(nombre)) {
			faltasEnFol.put(nombre, faltasEnFol.get(nombre)+1);
		}else {
			faltasEnFol.put(nombre, 1);
		}
		
		
		System.out.println(faltasEnFol.get("Bassou"));
		
		
		
		
	}

}
