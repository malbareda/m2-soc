package POO_Herencia;

public class Menjar {
	
	boolean recienHecho;
	double calorias;
	double preu;
	String nom;
	public Menjar(boolean recienHecho, double calorias, double preu, String nom) {
		super();
		this.recienHecho = recienHecho;
		this.calorias = calorias;
		this.preu = preu;
		this.nom = nom;
	}
	
	
	public Menjar(double calorias, double preu, String nom) {
		super();
		this.calorias = calorias;
		this.preu = preu;
		this.nom = nom;
		this.recienHecho=true;
	}
	public Menjar(String nom) {
		super();
		this.nom = nom;
		this.preu=0;
		this.calorias=0;
		this.recienHecho=true;
	}
	
	
	

}
