package POO_Herencia;

public class Panini extends Menjar{
	
	int longitud;
	
	
	public Panini(int longitud, String nom, boolean recienHecho, double calorias, double preu) {
		super(recienHecho, calorias, preu, nom);
		this.longitud = longitud;
	}
	
	public Panini() {
		super("panini");
	}

}
