package matrices;

import java.util.Scanner;

public class ComoMirarATuAlrededor {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int FILES = sc.nextInt();
		int COLUMNES = sc.nextInt();
		int[][] mat = new int[FILES][COLUMNES];
		
		
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[0].length;j++) {
				mat[i][j]=sc.nextInt();
			}
		}
		
		int f= sc.nextInt();
		int c= sc.nextInt();
		
				//arriba izquierda mat[f-1][c-1]
				//arriba mat[f-1][c]
				//arriba derecha mat[f-1][c+1]
				//izquierda mat[f][c-1]
				//tu posicion mat[f][c]
				//derecha mat[f][c+1]
				//abajo izwuierda mat[f+1][c-1]
				//abajo mat[f+1][c]
				//abajo derecha mat[f+1][c+1]
				
		int contadorVecinos = 0;
		
		
		if(estoyDentro(f-1,c-1,mat)&&mat[f-1][c-1]==1)contadorVecinos++;
		if(estoyDentro(f-1,c,mat)&&mat[f-1][c]==1)contadorVecinos++;
		if(estoyDentro(f-1,c+1,mat)&&mat[f-1][c+1]==1)contadorVecinos++;
		if(estoyDentro(f,c-1,mat)&&mat[f][c-1]==1)contadorVecinos++;
		if(estoyDentro(f,c+1,mat)&&mat[f][c+1]==1)contadorVecinos++;
		if(estoyDentro(f+1,c-1,mat)&&mat[f+1][c-1]==1)contadorVecinos++;
		if(estoyDentro(f+1,c,mat)&&mat[f+1][c]==1)contadorVecinos++;
		if(estoyDentro(f+1,c+1,mat)&&mat[f+1][c+1]==1)contadorVecinos++;
				
		System.out.println(contadorVecinos);
				
		
		
	}
	
	
	public static boolean estoyDentro(int f, int c, int[][] mat){
		if(f>=0 && c>=0 && f<mat.length && c<mat[0].length) {
			return true;
		}else {
			return false;
		}
	}

}
