package matrices;

import java.util.Random;

public class MatricesFilasYColumnas {

	public static void main(String[] args) {

		int[][] mat = new int[7][7];
		Random r = new Random();
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				mat[i][j] = r.nextInt(0, 10);
			}
		}

		imprimirMatrizDeInts(mat);

		// quiero la primera fila
		for (int j = 0; j < mat[0].length; j++) {
			//System.out.println(mat[0][j]);
		}
		
		int acc = 0;
		for (int j = 0; j < mat[0].length; j++) {
		     acc = acc + mat[0][j];
		}
		System.out.println(acc);
		
		acc=0;
		for (int i = 0; i < mat.length; i++) {
		     acc = acc + mat[i][0];
		}
		System.out.println(acc);
		
		
	}

	public static void imprimirMatrizDeInts(int[][] matrizFuncion) {

		for (int i = 0; i < matrizFuncion.length; i++) {
			for (int j = 0; j < matrizFuncion[0].length; j++) {
				System.out.print(matrizFuncion[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();

	}

}
