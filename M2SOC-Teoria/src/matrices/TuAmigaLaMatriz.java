package matrices;

import java.util.Random;

public class TuAmigaLaMatriz {
	
	public static void main(String[] args) {
		
		
		//array de arrays
		int filas = 4;
		int columnas = 3;
		int[][] matriz = new int[filas][columnas];
		
		Random r = new Random();
		
		//matriz.length -> el numero de filas
		//matriz[0].length -> el numero de columnas
		
		//el bucle de recorrer la matriz SIEMPRE ES ASI
		for(int i=0;i<matriz.length;i++) {
			for(int j=0;j<matriz[0].length;j++) {
				matriz[i][j] = r.nextInt(0,10);
			}
		}System.out.println();
		
		imprimirMatriz(matriz);
		
		matriz[0][0]=4;
		
		imprimirMatriz(matriz);
		matriz[1][1] = matriz[0][0]+1;
		imprimirMatriz(matriz);
		
		matriz[2][2] = matriz[1][1]-3;
		imprimirMatriz(matriz);
		
	}
	
	//Declarar Funcion
	//public static TIPO_DE_RETORNO NOMBRE_FUNCION( TIPO_PARAMETRO NOMBRE_PARAMETRO, TIPO_PARAMETRO2 NOMBRE_PARAMETRO2){
	//void implica que no deveulve nada
	public static void imprimirMatriz(int[][] matrizFuncion) {
		
		for(int i=0;i<matrizFuncion.length;i++) {
			for(int j=0;j<matrizFuncion[0].length;j++) {
				System.out.print(matrizFuncion[i][j]+" ");
			}System.out.println();
		}System.out.println();
		
		
	}
}
