package repaso;

public class RepasoIfs {
	
	public static void main(String[] args) {
		
		//if(BOOLEA){
		//   COSES QUE PASSEN SI EL BOOLEA ES TRUE
		// }else if(BOOLEA){
		//   COSES QUE PASSEN SI LA PRIMERA COMPROVACIO ES FALSA I LA SEGONA ES TRUE
		// ...
		// }else{
		//   COSES QUE PASSEN SI TOTES LES COMPROVACIONS SON FALSE
		// }
		
		/* TEST 1
		int a = 7;
		int b = 7;
		
		
		if(a>b) {
			System.out.println("POMA");
		}else {
			System.out.println("PERA");
		}
	
		*/
		/*
		//TEST2
		int a = 7;
		int b = 7;
		int c = 5;
		
		if(a>b) {
			System.out.println("POMA");
		}else if(b>a) {
			System.out.println("PERA");
		}
		
		System.out.println("adios");
		
		if(a>c) {
			System.out.println("PIÑA");
		}else if(b>c) {
			System.out.println("PLATAN");
		}else if(a==b) {
			System.out.println("PRUNA");
		}
		*/
		
		
		//TEST3
		int a = 7;
		int b = 7;
		int c = 5;
		
		if(a>b && b<a) {
			System.out.println("POMA");
		}else if(a==b || a!=b) {
			System.out.println("PERA");
		}
		
		
	}

}
