package repaso;

public class Variables {
	
	
	public static void main(String[] args) {
		//DECLARACION
		//tipo nombre = valor_inicial;
		//USO
		//nombre = nuevo_valor; (una vez declarada nunca mas se vuelve a poner el tipo)
		int a = 5;
		a = 8;
		//las variables se pueden usar a si mismo
		a = (a-a)*a; //(8-8)*8
		System.out.println(a);
		a = (a+a)*a; //(0+0)*0;
	}

}
