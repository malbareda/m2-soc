package bfs;

public class BFS {
	 
	static int[][] lab = {
			{0,0,0,0,0},
			{0,1,0,1,2},
			{0,1,1,1,0},
			{0,1,0,0,0},
			{0,1,0,1,0},
			{0,1,0,1,1},
	};
	
	
	public static void main(String[] args) {
		
		
		
		int fini = 0;
		int cini = 0;
		
		System.out.println(encontrarSalida(fini,cini));
		
		
		
	}
	
	public static boolean encontrarSalida(int f, int c) {
		if(f<0||c<0|f>=lab.length||c>=lab[0].length) {
			return false; //estoy fuera de la matriz
		}else if(lab[f][c]==1) {
			return false; //estoy en una pared
		}else if(lab[f][c]==2) {
			return true; //estoy justo encima de la salida
		}
		else {

			lab[f][c]=1;//por donde ya he pasado lo bloqueo
		
			boolean n = encontrarSalida(f-1,c);
			boolean e = encontrarSalida(f,c+1);
			boolean o = encontrarSalida(f,c-1);
			boolean s = encontrarSalida(f+1,c);
			
			return n||e||o||s;
		}
	}

}
