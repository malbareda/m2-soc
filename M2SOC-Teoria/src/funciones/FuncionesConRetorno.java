package funciones;

public class FuncionesConRetorno {
	
	public static void main(String[] args) {
		
		int res = multiplicaPor2(5);
		System.out.println(res);
		
	}

	private static int multiplicaPor2(int i) {
		
		int resultado = i*2;
		//devuelve esto
		return resultado;
		
	}

}
