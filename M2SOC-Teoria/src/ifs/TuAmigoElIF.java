package ifs;

import java.util.Scanner;

public class TuAmigoElIF {
	
	public static void main(String[] args) {
		
		//estructura del if
		
		// if(BOOLEANO){
		//  cosas que quieres hacer si se cumple la condicion;
		//  mas cosas que quieres hacer;
		//  etc
		// }else{
		//   cosas que quieres hacer si NO se cumple la condicion
		//   mas cosas que queires hacer si NO se cumple
		// }
		
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		if(b==0) {
			//si b es igual a 0
			System.out.println("no se puede dividir por cero");
		}else {
			//si b NO es igual a 0
			int div = a/b;
			System.out.println(div);	
		}
		
		
		
	}

}
