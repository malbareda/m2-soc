package ifs;

import java.util.Scanner;

public class TuAmigoElSwitch {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String dia = sc.nextLine();
		/* Forma larga
		if(dia.equals("Lunes") || dia.equals("LUNES")) {//el equals SOLO con las Strings
			System.out.println(1);
		}else if(dia.equals("Martes")) {
			System.out.println(2);
		}//...
		*/
		
		
		switch(dia) {
		case "Lunes":
		case "lunes":
		case "LUNES":
			System.out.println(1);
			break;
		case "MARTES":
		case "martes":
		case "Martes":
			System.out.println(2);
			break;
		case "MIERCOLES":
		case "miercoles":
		case "Miercoles":
			System.out.println(3);
			break;
		case "JUEVES":
		case "jueves":
		case "Jueves":
			System.out.println(4);
			break;
		case "Viernes":
			System.out.println(5);
			break;
		case "Sabado":
			System.out.println(6);
			break;
		case "Domingo":
			System.out.println(7);
			break;
		default:
			System.out.println("ERROR");
		}
		
		
	}

}
