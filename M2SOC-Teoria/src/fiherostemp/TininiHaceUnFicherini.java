package fiherostemp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TininiHaceUnFicherini {
	
	public static void main(String[] args) throws IOException {
		
		
		int a = damePuntos("Tinini");
		if(a==-1) {
			insertarNombre("Tinini");
		}
		System.out.println(a);
		
	}

	private static void insertarNombre(String string) throws IOException {
		// TODO Auto-generated method stub
		File f = new File("algo.txt");
		FileWriter fw = new FileWriter(f,true);//modo append
		BufferedWriter bw = new BufferedWriter(fw);	
		
		bw.write(string+";"+0);
		bw.newLine();
		bw.flush();
		bw.close();
		
	}

	private static int damePuntos(String string) throws IOException {
		File f = new File("algo.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);	
		
		boolean encontrado = false;
		
		while(br.ready()) {
			String linea = br.readLine();
			String nombre = linea.split(";")[0];
			int puntos = Integer.parseInt(linea.split(";")[1]);
			if(nombre.equals(string)) {
				encontrado=true;
				br.close();
				return puntos;
			}
		}
		//aqui llegas solo cuando llegas al final de fichero
		
		br.close();
		return -1;
		
		
		
	}

}
