package fiherostemp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicherosTexto {
	
	
	public static void main(String[] args) throws IOException {
		escribirtexto();
		leertexto();
		
		
	}

	private static void leertexto() throws IOException {
		// TODO Auto-generated method stub
		File f = new File("algo.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);	
		
		
		while(br.ready()) {
			System.out.println(br.readLine());
		}
	}

	private static void escribirtexto() throws IOException {
		// TODO Auto-generated method stub
		File f = new File("algo.txt");
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);	
		
		
		bw.write("hola");
		bw.newLine();
		bw.write("cosas");
		bw.newLine();
		bw.write("mas cosas");
		bw.newLine();
		bw.write("fin");
		
		bw.flush();
	}
	

}
