package bucles;

import java.util.Scanner;

public class BuclesOtraVez {
	
	public static void main(String[] args) {
		
		//tipo de entrada:
		//te dicen un numero n y luego te entran n numeros
		//variables:
		//contador de positivos
		
		int positivos = 0;
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		while(n>0) {
			n--;
			int numero = sc.nextInt(); //esto es para cada caso. Esto es un caso individual. Solo ele un numero, la gracia es que todo esto lo haces n veces
			if(numero>0) {//es positivo
				positivos++;
			}
		}
		System.out.println(positivos);
		
		
		
		
		
	}

}
