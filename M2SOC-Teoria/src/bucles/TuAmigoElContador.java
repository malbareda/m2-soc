package bucles;

import java.util.Scanner;

public class TuAmigoElContador {
	
	public static void main(String[] args) {
		
		
		//WHILE
		//while(condicion booleana){
			//CODIGO SI ES TRUE
			//MAS CODIGO SI ES TRU
		//}
		//NO HAY ELSE NI NADA POR EL ESTILO
		
		Scanner sc = new Scanner(System.in);
		//leer un numero y contar n veces
		System.out.println("leer un numero y contar n veces");
		int n = sc.nextInt();
		
		while(n>0) {//hasta que n sea mayor que 0
			n--;//decrementar
			
			//AQUI VENDRIA EL CODIGO DE LO QUE QUIERES HCAER
			System.out.println("miau");
			
		}
		
		//YO LEO UN NUMERO Y DIGO MIAU HASTA QUE ESCRIBO 0
		
		System.out.println("leer un numero y decir miau hasta que escribo 0");
		int num = sc.nextInt(); //la primera vez lo leemos FUERA DEL BUCLE
		while(num!=0) {//condicion
			//num = sc.nextInt();
			//AQUI HAGO EL CODIGO QUE QUIERO HACER
			System.out.println(num);
			
			//
			num = sc.nextInt();
			
		}
		
		//UN CONTADOR ES UNA VARIABLE QUE SOLO SIRVE PARA CONTAR (O DESCONTAR)
		//ENTONCES TU LA DECLARAS Y SOLO LA MODIFICAS CON UN c++;  o c--;
		
		System.out.println("leer un numero y contar n notas y decir cuantas estan aprobadas");
		
		int n1 = sc.nextInt();
		int contadorNotas1=0;
		while(n1>0) {//hasta que n sea mayor que 0
			n1--;//decrementar
			
			//AQUI VENDRIA EL CODIGO DE LO QUE QUIERES HCAER
			int nota1 = sc.nextInt();
			if(nota1>=5) {
				contadorNotas1++;//incremento el contador
			}
			
		}
		//AL FINAL DEL BUCLE, UNA VEZ HA ACABADO
		System.out.println(contadorNotas1);
		
		
		
		System.out.println("leer notas hasta que ponga -1 y decir cuantas estan aprobadas");
		int contadorNotas2 = 0;
		int nota2 = sc.nextInt(); //la primera vez lo leemos FUERA DEL BUCLE
		while(nota2!=-1) {//condicion
			//num = sc.nextInt();
			//AQUI HAGO EL CODIGO QUE QUIERO HACER
			if(nota2>=5) {
				contadorNotas2++;
			}
			
			//
			nota2 = sc.nextInt();
			
		}
		//AL FINAL DEL BUCLE
		System.out.println("tienes "+contadorNotas2+" aprobados");
		
	}

}
