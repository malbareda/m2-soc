package bucles;

import java.util.Random;

public class RepasoBucles {

	public static void main(String[] args) {
		
		//while //no sabemos exactamente cuantas veces se repetira el bucle
		//for	 //sabemos cuantas veces se repite el bucle
		
		//el while funciona como un if
		
		Random r = new Random();
		int random = r.nextInt(0,10);
		while(random>0) {
			//se repetira lo que hay dentro mientras la condicion del while sea true
			System.out.println("hemos sacado un "+random);
			random = r.nextInt(0,10);

		}
		System.out.println("he sacado un 0, se acaba el programa");
			
		//for es un bucle que sabes cuantas veces se repite
		int veces = 5;
		for(int i=0;i<veces;i++) {
			System.out.println("algo");
		}
	}
}
