package bucles;

import java.util.Scanner;

public class Bucles {
	
	public static void main(String[] args) {
		//1. whiles
		//2. fors
		//2.5 do-while 
		//3. contador
		//4. acumulador
		//5. flag
		
		
		//bucle -> repetir la misma cosa varias veces
		//while(condicion booleana){
		//    este codigo se ejecuta mientras la condicion sea true
		// }
		// los while no tienen else
		
		int a = 5;
		while(a>0) {
			System.out.println("miau");
			a--;
		}
		System.out.println("guau");
		
		
		//contador
		
		int contador = 0;
		int contador10 = 0; //contador de cuantos numeros son iguales o mayores que 10
		Scanner sc = new Scanner(System.in);
		while(contador<10) {
			int numero = sc.nextInt();
			System.out.println(numero);
			if(numero>10) {
				contador10++;
			}
			contador++;
			//a++ => a = a+1
		}
		System.out.println("de esos 10 numeros que leido "+contador10+ "son amyores que 10");
		System.out.println("fin");
		
	}

}
