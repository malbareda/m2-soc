package bucles;

import java.util.Scanner;

public class acumulador {
	
	public static void main(String[] args) {
		
		
		//el acumulador acumula resultados
		
		//acc = acc + algo
		//acc = acc - algo
		
		//pongo n, me llegan n numeros, y tengo que devolver la suma de todos
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		int acc = 0;
		for(int i=0;i<n;i++) {
			
			int numero = sc.nextInt();
			
			acc = acc + numero;
			
		}
		System.out.println(acc);
		
		
	}

}
