package bucles;

import java.util.Scanner;

public class TuAmigoElFor {
	
	public static void main(String[] args) {
		
		//for(int i = <inicio>; condicion de mantenerse en el bucle; step){
		
		
		//for que cuente del 1 al 10
		
		for(int i = 1; i<=10; i++) {
			System.out.println("miaus"+i);
		}
		
		//for que cuente del 20 al 10
		for(int i = 20; i >= 10; i--) {
			System.out.println("guaus"+i);
		}
		
		
		//leo un numero n y luego me llegan n numeros
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for(int i = 0; i<n; i++) {
			//aqui leer numeros
		}
		
	}

}
