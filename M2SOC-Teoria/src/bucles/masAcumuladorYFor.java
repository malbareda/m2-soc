package bucles;

import java.util.Scanner;

public class masAcumuladorYFor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//acumulador
		//acumulador = acumulador + algo
		
		//for
		//for(int i = <valor inicial>; <condicion de estar dentro del bucle> ; aumento o decremento de la i){
		
		//5 elevado a 8
		
		//n elevado a m
		int n = 5;
		int m = 8;
		int acc = 1;//si el acumulador es multiplicativo
		
		for(int i =0;i<m;i++) {
			acc = acc * n;
		}
		System.out.println(acc);
		
		
		int capacidad = 1000;
		int nAutobuses = 10;
		
		for(int i=0;i<nAutobuses;i++) {
			int nTuristas = sc.nextInt();
			capacidad = capacidad - nTuristas;
			if(capacidad<=0) {
				System.out.println("El museo se ha lleanado en el autobus numero "+i);
			}
		}
		
		
		
	}
}
