package funcionesTopDown;

import java.util.Scanner;

public class TresEnRaya {
	
	//ahora sc es una variable global
	//una variable es global si la ponees FUERA DEL MAIN y con static delante
	static Scanner sc = new Scanner(System.in);

	
	public static void main(String[] args) {
		
		
		int[][] tablero = new int[3][3];
		int filaElegida;
		int columnaElegida;
		int turno=1;
		
		//el j1 escoge una casilla
		//se pone una ficha del j1 (si esta vacio y dentro de la matriz)
		//comprobar si hay un 3 en raya
		//comprobar si el tablero se ha llenado
		//cambiar el turno
		boolean hay3EnRaya=false;
		boolean tableroLleno=false;
		
		while(!tableroLleno && !hay3EnRaya) {
			System.out.println("turno del jugador "+turno);
			System.out.println("escoge una fila");
			filaElegida = escogerFila();
			System.out.println("escoge una columna");
			columnaElegida = escogerColumna();
			tablero = ponerFicha(filaElegida,columnaElegida,tablero,turno);
			hay3EnRaya = comprobarSiHay3EnRaya(tablero, turno);
			tableroLleno = tableroLleno(tablero);
			if(!hay3EnRaya) {
				turno = cambiarTurno(turno);
			}
			imprimirTablero(tablero);
		}
		if(hay3EnRaya) {
			System.out.println("ha ganado "+turno);
		}
		if(tableroLleno) {
			System.out.println("empate");
		}
		
		
		
	}

	private static void imprimirTablero(int[][] tablero) {
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				System.out.print(tablero[i][j]+" ");
			}
			System.out.println();
		}
	}

	private static int cambiarTurno(int turno) {
		if(turno==2) {
			return 1;
		}else {
			return 2;
		}
	}

	private static boolean tableroLleno(int[][] tablero) {
		//recorrer matriz
		int contador0 = 0;
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				if(tablero[i][j]==0) {
					contador0++;
				}
			}
		}
		if(contador0==0) {
			return true;
		}else {
			return false;
		}
	}

	private static boolean comprobarSiHay3EnRaya(int[][] tablero, int turno) {
		
		if(tablero[0][0]==turno && tablero[0][1] == turno && tablero[0][2]==turno) {
			return true;
		}else if(tablero[1][0]==turno && tablero[1][1] == turno && tablero[1][2]==turno) {
			return true;
		}else if(tablero[2][0]==turno && tablero[2][1] == turno && tablero[2][2]==turno) {
			return true;
		}else if(tablero[0][0]==turno && tablero[1][0] == turno && tablero[2][0]==turno) {
			return true;
		}else if(tablero[0][1]==turno && tablero[1][1] == turno && tablero[2][1]==turno) {
			return true;
		}else if(tablero[0][2]==turno && tablero[1][2] == turno && tablero[2][2]==turno) {
			return true;
		}else if(tablero[0][0]==turno && tablero[1][1] == turno && tablero[2][2]==turno) {
			return true;
		}else if(tablero[0][2]==turno && tablero[1][1] == turno && tablero[2][0]==turno) {
			return true;
		}else {
			return false;
		}
		
		
	}

	private static int[][] ponerFicha(int f, int c, int[][] tablero, int turno) {
		//ver si puedes meterlo
		if(f<3 && f>=0 && c<3 && c>=0) {
			//ver si no esta ocupado
			if(tablero[f][c]==0) {
				tablero[f][c]=turno;
			}
		}
		return tablero;
	}

	private static int escogerColumna() {
		int c = sc.nextInt();
		return c;
	}

	private static int escogerFila() {
		int f = sc.nextInt();
		return f;
	}

}
