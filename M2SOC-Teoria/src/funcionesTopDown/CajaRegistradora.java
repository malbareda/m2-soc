package funcionesTopDown;

import java.util.ArrayList;
import java.util.Scanner;

public class CajaRegistradora {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		boolean salir = false;

		int precioTotal = 0;
		int precioUnidad = 0;
		int cantidad;
		ArrayList<String> listaProductos = new ArrayList<String>();

		while (!salir) {
			System.out.println("introduce tu opcion");
			System.out.println("1: Introducir un producto");
			System.out.println("2: Multiplicar el ultimo producto por una cantidad");
			System.out.println("3: Acabar la cuenta y sumar");
			System.out.println("4: Calcular el cambio");
			System.out.println("5: Borrar el ultimo producto");
			System.out.println("6: Aplicar un % de descuento");
			System.out.println("7: Salir");

			int opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				System.out.println("Introducir un producto");
				//// TODO
				System.out.println("Introduce el Nombre");
				String nombre = sc.nextLine();
				listaProductos.add(nombre);
				System.out.println("Introduce el precio de la unidad");
				precioUnidad = sc.nextInt();
				sc.nextLine();
				// nos hemos olvidado de una cosita
				precioTotal = precioTotal + precioUnidad;

				break;
			case 2:
				System.out.println("Multiplicar el ultimo producto por una cantidad");
				if (precioUnidad == 0) {
					System.out.println("Debes scannear algo antes de multiplicarlo");
				} else {
					System.out.println("Cuantas Unidades del último producto scanneado?");
					int numeroUnidades = sc.nextInt();
					int multiplicacion = numeroUnidades * precioUnidad;
					precioTotal = precioTotal + ((numeroUnidades-1) * precioUnidad);
				}
				break;
			case 3:
				System.out.println("Acabar la cuenta y sumar");
				System.out.println("Tu compra es: ");
				System.out.println(listaProductos);
				System.out.println("Precio: " + precioTotal);
				System.out.println("Le ha atendido Sergio L");
				break;
			case 4:
				System.out.println("Calcular el cambio");
				System.out.println("Dime cuanto dinero te han dado");
				int dinero = sc.nextInt();
				if (dinero > precioTotal) {
					int resta = dinero - precioTotal;
					System.out.println("El cambio es " + resta);
				} else {
					System.out.println("Vete a ser pobre a otra parte");
				}
				break;
			case 5:
				System.out.println("Borrar el ultimo producto");
				break;
			case 6:
				System.out.println("Aplicar un % de descuento");
				break;
			case 7:
				salir = true;
				break;
			}

		}

	}

}
