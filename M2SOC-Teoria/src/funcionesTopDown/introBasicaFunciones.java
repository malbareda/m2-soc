package funcionesTopDown;

import java.util.Scanner;

public class introBasicaFunciones {
	
	public static void main(String[] args) {
		
		//funcion que no le entra nada ni sale nada
			//danielTinini();
			//danielTinini();
			
		//funcion a la cual le entra algo
			//danielTinini2("Sergio L ()");
			
			//danielTinini3("panini","daniel tinini");
		//el orden importa
			//danielTinini4(4,"panini","daniel tinini");
			
			//las funciones que devuelven algo se llaman de manera distinta
			//TIPO variable = funcionQueDeveulveTIPO()
			
			int numPaninis = cuantosPaninisSeHaComidoDanielTinini();
			System.out.println(numPaninis);
			
			Scanner sc = new Scanner(System.in);
			
			System.out.println("di un numero");
			int numQuizasPar = sc.nextInt();
			boolean par = esPar(numQuizasPar);
			System.out.println(par);
			
			
			int cuad = cuadrado(sc.nextInt());
			System.out.println(cuad);
			
			
			String comida = queHayDeComer("Sergio L");
			System.out.println(comida);
		
		
	}


	private static String queHayDeComer(String string) {
		if(string.equals("Daniel Tinini")){
			return "Panini";
		}else {
			return "Lentejas";
		}
	}


	private static int cuadrado(int i) {
		return i*i;
	}


	private static boolean esPar(int i) {
		if(i%2==0) {
			//es par
			return true;
		}else {
			//es impar
			return false;
		}
	}


	private static int cuantosPaninisSeHaComidoDanielTinini() {
		return 1;
	}


	private static void danielTinini4(int veces, String string, String string2) {
		// TODO Auto-generated method stub
		for(int i=0;i<veces;i++) {
			System.out.println(string+" se come un "+string2);
		}
	}

	private static void danielTinini3(String string, String string2) {
		System.out.println(string+" se come un "+string2);
	}

	private static void danielTinini2(String a) {
		System.out.println("Daniel Tinini se come un "+a);
	}

	private static void danielTinini() {
		// TODO Auto-generated method stub
		System.out.println("se come un panini");
	}

}
