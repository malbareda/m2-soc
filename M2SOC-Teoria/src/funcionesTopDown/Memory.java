package funcionesTopDown;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Memory {
	
	public static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int[][] tableroPublico = new int[4][4];
		int[][] tableroSecreto = new int[4][4];

		//2 jugadores
		int turno = 1;
		int contadorParejas1 = 0;
		int contadorParejas2 = 0;
		int filaElegida;
		int columnaElegida;
		
		//iniciar la matriz
		tableroSecreto = iniciar();
		while(contadorParejas1+contadorParejas2<8) {
			System.out.println("turno del jugador "+turno);
			System.out.println("escoge una fila");
			filaElegida = escogerFila();
			System.out.println("escoge una columna");
			columnaElegida = escogerColumna();
			tableroPublico = imprimirPosicionElegida(filaElegida,columnaElegida,tableroSecreto,tableroPublico);
			imprimirTablero(tableroPublico);
			System.out.println("-----------------");
			imprimirTablero(tableroSecreto);
			System.out.println("escoge otra fila");
			int filaElegida2 = escogerFila();
			System.out.println("escoge otra columna");
			int columnaElegida2 = escogerColumna();
			tableroPublico = imprimirPosicionElegida(filaElegida2,columnaElegida2,tableroSecreto,tableroPublico);
			imprimirTablero(tableroPublico);
			System.out.println("-----------------");
			imprimirTablero(tableroSecreto);
			boolean pareja = comparar(tableroSecreto,filaElegida,columnaElegida,filaElegida2,columnaElegida2);
			if(!pareja) {
				System.out.println("no son pareja");
				turno = cambiarTurno(turno);
				tableroPublico = volverATapar(tableroPublico,filaElegida,columnaElegida,filaElegida2,columnaElegida2);
			}else {
				System.out.println("si son pareja");
				tableroSecreto = volverATapar(tableroSecreto,filaElegida,columnaElegida,filaElegida2,columnaElegida2);
			}
		}
		
		
		
		
	}

	private static int[][] volverATapar(int[][] tableroPublico, int filaElegida, int columnaElegida, int filaElegida2,
			int columnaElegida2) {
		tableroPublico[filaElegida][columnaElegida]=0;
		tableroPublico[filaElegida2][columnaElegida2]=0;
		return tableroPublico;
	}

	private static int[][] iniciar() {
		ArrayList<Integer> lista = new ArrayList<Integer>();
		
		for(int i=1;i<=8;i++) {
			lista.add(i);
			lista.add(i);
		}
		
		System.out.println(lista);
		
		Collections.shuffle(lista);
		
		int[][] tableroSecreto = new int[4][4];
		
		int contadorLista=0;
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				tableroSecreto[i][j]=lista.get(contadorLista);
				contadorLista++;
			}
		}
		return tableroSecreto;

	}

	private static void imprimirTablero(int[][] tableroPublico) {
		// TODO Auto-generated method stub
		for(int i=0;i<tableroPublico.length;i++) {
			for(int j=0;j<tableroPublico[0].length;j++) {
				System.out.print(tableroPublico[i][j]+" ");
			}System.out.println();
		}
	}

	private static int cambiarTurno(int turno) {
		if(turno==1) {
			return 2;
		}else {
			return 1;
		}
	}

	private static boolean comparar(int[][] tableroSecreto, int filaElegida, int columnaElegida, int filaElegida2, int columnaElegida2) {
		if(tableroSecreto[filaElegida][columnaElegida]==tableroSecreto[filaElegida2][columnaElegida2]&&tableroSecreto[filaElegida][columnaElegida]!=0&&tableroSecreto[filaElegida2][columnaElegida2]!=0) {
			return true;
		}else {
			return false;
		}
	}

	private static int[][] imprimirPosicionElegida(int filaElegida, int columnaElegida, int[][] tableroSecreto, int[][] tableroPublico) {
		tableroPublico[filaElegida][columnaElegida]=tableroSecreto[filaElegida][columnaElegida];
		return tableroPublico;
	}

	private static int escogerColumna() {
		boolean valido = false;
		int f = 0;
		while(!valido) {
			f = sc.nextInt();
			if(f>=0 && f<4) {
				valido=true;
			
			}
		}
		return f;
	}

	private static int escogerFila() {
		boolean valido = false;
		int f = 0;
		while(!valido) {
			f = sc.nextInt();
			if(f>=0 && f<4) {
				valido=true;
			
			}
		}
		return f;
		
	}

}
