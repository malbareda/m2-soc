package Introduccion;

import java.util.Scanner;

public class TiposDeEntradaDeScanner {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		String texto = sc.nextLine(); //lee una linea (lee hasta que pulsas enter)
		System.out.println("la primera liena es "+texto);
		texto = sc.nextLine(); //lee una linea (lee hasta que pulsas enter)
		System.out.println("la segunda liena es "+texto);
		
		int a = sc.nextInt(); //leer int
		double b = sc.nextDouble(); //leer double
		boolean c = sc.nextBoolean(); //leer booleano
		
		
	}

}
