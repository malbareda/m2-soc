package lista;

import java.util.ArrayList;
import java.util.Collections;

public class MasCosasDeListas {
	
	public static void main(String[] args) {
		
		
		ArrayList<String> Sergios = new ArrayList<>();
		ArrayList<String> Danis = new ArrayList<>();
		
		
		Sergios.add("Musica");
		Sergios.add("Anime");
		Sergios.add("Programacion");
		Sergios.add("Bussi");
		
		Danis.add("Videojuegos");
		Danis.add("Netflix");
		Danis.add("Grindr");
		Danis.add("Bussi");
		
		System.out.println(Sergios);
		
		//indexOf
		System.out.println(Sergios.indexOf("Sergio que hace caso al profesor"));
		
		int posicion = -1;
		for(int i = 0; i<Sergios.size();i++) {
			if(Sergios.get(i).equals("Sergio que pasa de todo")){
				posicion = i;
			}
		}
		System.out.println(posicion);
		
		/*
		 * get
		 * set
		 * add (2 tipos de add)
		 * remove (2 tipos de remove)
		 * indexOf
		 * contains
		 * clear -- borra la lista
		 * Collections.sort
		 * Collections.reverse
		 * 
		 */
	
		Collections.sort(Sergios);
		
		System.out.println(Sergios);
		System.out.println(Danis);
		
		//Sergios.addAll(Danis); //añade a la primera lista todos los elementos de la segunda lsita
		//Sergios.removeAll(Danis); quita de la primera lista, todos los elementos comunes en las dos listas
		//Sergios.retainAll(Danis); //devuelve todos los elementos comunes en las dos listas
		
		System.out.println(Sergios);
		
		
		String s="Bussi";
		
		
		
		
		
	}

}
